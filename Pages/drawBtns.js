function addCharacters(dispDiv, chars) {
    var disp = document.getElementById(dispDiv)
    chars.forEach(char => {
        var btn = document.createElement("button")
        var txt = document.createTextNode(char.font)
        btn.onclick = function() {displayChar(char)}
        btn.className = "btn btn-light translated charBtn rounded-0"
        btn.appendChild(txt)
        disp.appendChild(btn)
    })
}

function displayChar(char) {

    title = document.getElementById("charTitle")
    title.innerHTML = `<span class="translated">${char.char}</span`

    imgs = document.getElementById("charImgs")
    imgs.innerHTML = ""
    for(i = 0; i < char.pngCount; i++) {
        var thisImg = document.createElement("img")
        thisImg.className = "charDisp2"
        
        var altName = ""
        for(k = 0; k < i; k++) {altName = altName + "_"}
        thisImg.src = "../images/alphabetical_glyphs/" + char.char + altName +".png"

        imgs.appendChild(thisImg)
    }

    table = document.getElementById("charInfo")
    table.innerHTML = ""
    tr = document.createElement("tr")

    tdSpacing = document.createElement("td")
    tdSpacing.className = "SpacingLight"
    tr.appendChild(tdSpacing)

    tdRep = document.createElement("td")
    pRep = document.createElement("p")
    pRep.className = "letter"
    pRep.innerHTML = char.rep
    tdRep.appendChild(pRep)
    tr.appendChild(tdRep)

    vSep = document.createElement("td")
    vSep.className = "vSep"
    tr.appendChild(tdSpacing.cloneNode(true))
    tr.appendChild(vSep)
    tr.appendChild(tdSpacing.cloneNode(true))

    tdLefty1 = document.createElement("td")
    tdLefty1.className = "lefty"
    tdLefty1.innerHTML = "<p>Pronunciation</p>"
    tr.appendChild(tdLefty1)

    tdRighty1 = document.createElement("td")
    tdRighty1.className = "righty"
    char.pronunciations.forEach(pronun => {
        pTemp = document.createElement("p")
        pTemp.innerHTML = "["+pronun+"]"
        tdRighty1.appendChild(pTemp)
    })
    tr.appendChild(tdRighty1)

    tr.appendChild(tdSpacing.cloneNode(true))
    tr.appendChild(vSep.cloneNode(true))
    tr.appendChild(tdSpacing.cloneNode(true))

    tdLefty2 = document.createElement("td")
    tdLefty2.className = "lefty"
    tdLefty2.innerHTML = "<p>Aspects</p>"
    tr.appendChild(tdLefty2)

    tdRighty2 = document.createElement("td")
    tdRighty2.className = "righty"
    char.aspects.forEach(aspect => {
        pTemp = document.createElement("p")
        pTemp.innerHTML = aspect
        tdRighty2.appendChild(pTemp)
    })
    tr.appendChild(tdRighty2)

    table.appendChild(tr)

    //This last bit makes the window scroll down for good effect
    //scrollWindow(300)
    window.scrollTo(0,document.body.scrollHeight)
}

function scrollWindow(iterations) {
    if(iterations >= 0) {
        window.scrollBy(0,2)
        scrolldelay = setTimeout(function() {scrollWindow(iterations-1)},1)
    }
}
