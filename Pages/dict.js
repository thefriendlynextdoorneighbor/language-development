// I know this is not an ideal way to list all the words I have, but gitlab
// was not making it easy for me to access all files in a directory. In the
// future I may replace this with a script that writes a list of all words
// to a json file that I can query directly, then I won't have to manually
// fill out the list.
engWords = [
    "desire",
    "fire",
    "heat",
    "speak",
    "talk",
    "want",
    "water",
    "word",
    "blue",
    "red",
    "green",
    "yellow",
].sort()
kamWords = [
    "aBb",
    "aBk",
    "BeL",
    "BlL",
    "BmL",
    "GRoN",
    "HfR",
    "KeLtaN",
    "MaRa",
    "MaRaK",
    "ReLD",
    "ReLi",
    "ReLTeN",
    "ReLTjN",
    "WkN",
    "siK",
    "ClxaK",
    "DozaK",
    "PezaK",
    "WeWeK",
    "BaMBiK",
].sort()

function searchEnglish(query) {
    
    var searchParams = new URLSearchParams(window.location.search)
    searchParams.set("word", query)
    console.log()
    window.location.search = searchParams.toString()
}

// Load English Definitions
function loadDefn(query, isEnglish) {
    var location = "Definitions/" + (isEnglish ? "English/" : "Kamara/")
    console.log(location + query + ".json")
    $.ajax({
        url: location + query + ".json",
        dataType: "json",
        type: "Get",
        cache: false,

        success: function(data) {
            displayDefn(data)
        },

        error: function(data) {
            console.error("Failure")
            console.log(data)
        },
    }).responseText
}

function displayDefn(data) {
    // Gets a reference to the <p> tag that will contain the entry and clears the current entry
    var container = document.getElementById("searchResult")
    container.innerHTML = ""

    createEntryBody(data, container, "plus")

    // This block will add all the translations of the word to the searchResult element
    translations = document.createElement("div")
    translations.className = "widthInherit rowDisp"
    translations.innerHTML = "<p>&emsp;&emsp;</p>"
    translationsRight = document.createElement("div")

    data.translations.forEach((translation) => {
        $.ajax({
            // Searches for the json files of the translations
            url: "Definitions/" + ((data.lang === "km") ? "English/" : "") + ((data.lang == "en") ? "Kamara/" : "") + translation + ".json",
            dataType: "json",
            typr: "get",
            cache: false,

            // If the translation is found then it will be displayed
            success: function(transData) {
                thisTranslation = document.createElement("div")
                thisTranslation.className = "widthInherit"
                createEntryBody(transData, thisTranslation, "")
                translationsRight.appendChild(thisTranslation)
            },

            // If a translation is not found then only the supplied word will be
            error: function() {
                thisTranslation = document.createElement("div")
                transWord = document.createElement("p")
                transWord.innerHTML = translation.charAt(0).toUpperCase() + translation.slice(1)
                thisTranslation.appendChild(transWord)
                translationsRight.appendChild(thisTranslation)
            }
        })
        
    })
    translations.appendChild(translationsRight)
    container.appendChild(translations)
}

/*
    createEntryBody creates the body of text for the given entry and adds it to the given location
*/
function createEntryBody(data, container, classType) {
    // Creates the translated title for the section and states its type
    transWord = document.createElement("p")
    transWord.className = `classType ${((data.lang === "km") ? "translated" : "")}`
    transWord.innerHTML = data.word
    container.appendChild(transWord)

    // This forEach will add in each of the translation's articles
    data.body.forEach((article) => {
        // Adds the word type/ part of speech
        transWordType = document.createElement("p")
        transWordType.innerHTML = "<small><i>&ensp;" + article.class + "</i></small>"
        container.appendChild(transWordType)
        
        articleElem = document.createElement("p")
        articleElem.innerHTML = "<small>&emsp;" + article.text + "</small>"
        container.appendChild(articleElem)
    })
}

function listWords(words, container, isEnglish) {
    words.forEach(word => {
        wordWrapper = document.createElement("p")
        wordA = document.createElement("a")
        wordA.href = `${isEnglish ? "EnglishDict.html" : "KamaranDict.html"}?word=${word}`
        wordA.innerHTML = word
        if (!isEnglish) {wordA.className = "translated"}wordWrapper
        wordWrapper.appendChild(wordA)
        container.appendChild(wordWrapper)
    })
}