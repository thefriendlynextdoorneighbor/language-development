/* 
 * Written (poorly (very)) by Ryan Druffel with the intention of cleaning it up later
 */

 // These two are for determining the language used in text
const en = 'en'
const km = 'km'

function searchForEn() {
    loadEntry(document.getElementById("SearchBarEn").value, en)
}

function searchForKm() {
    loadEntry(document.getElementById("SearchBarKm").value, km)
}

function searchFor(language) {
    searchText = document.getElementById("SearchBar").value
    console.log(language)
    loadEntry(searchText, language)
}

function loadEntry(searchTerm, language) {

    // This sets the correct directory for the search term based on if it is english or Kamara
    var location = "Definitions/" + ((language === "en") ? "English/" : "") + ((language == "km") ? "Kamara/" : "")
    console.log(location + searchTerm + ".json")

    $.ajax({
        // Searches for a json file with the search term as the name of the file
        url: location + searchTerm + ".json",
        dataType: "json",
        typr: "get",
        cache: false,

        // Displays the entry found for the search term
        success: function(data) {
            displayEntry(data)
        },

        // Alerts the user that the expected entry was not able to be found
        error: function() {
            displayFailedEntry()
        }
    }).responseText
}

/*
    display entry takes the retrieved json file and displays it in the searchResult paragraph
*/
function displayEntry(data) {
    // Gets a reference to the <p> tag that will contain the entry and clears the current entry
    var container = document.getElementById("searchResult")
    container.innerHTML = ""

    createEntryBody(data, container, "plus")

    // This block will add all the translations of the word to the searchResult element
    translations = document.createElement("div")
    translations.className = "widthInherit rowDisp"
    translations.innerHTML = "<p>&emsp;&emsp;</p>"
    translationsRight = document.createElement("div")

    data.translations.forEach((translation) => {
        $.ajax({
            // Searches for the json files of the translations
            url: "Definitions/" + ((data.lang === "km") ? "English/" : "") + ((data.lang == "en") ? "Kamara/" : "") + translation + ".json",
            dataType: "json",
            typr: "get",
            cache: false,

            // If the translation is found then it will be displayed
            success: function(transData) {
                thisTranslation = document.createElement("div")
                thisTranslation.className = "widthInherit"
                createEntryBody(transData, thisTranslation, "")
                translationsRight.appendChild(thisTranslation)
            },

            // If a translation is not found then only the supplied word will be
            error: function() {
                thisTranslation = document.createElement("div")
                transWord = document.createElement("p")
                transWord.className = ((data.lang === km) ? "" : "translated")
                transWord.innerHTML = translation.charAt(0).toUpperCase() + translation.slice(1)
                thisTranslation.appendChild(transWord)
                translationsRight.appendChild(thisTranslation)
                translationsRight.appendChild(document.createElement("hr"))
            }
        })
        
    })
    translations.appendChild(translationsRight)
    container.appendChild(translations)
}

/*
    createEntryBody creates the body of text for the given entry and adds it to the given location
*/
function createEntryBody(data, container, classType) {
    // Creates the translated title for the section and states its type
    transWord = document.createElement("p")
    transWord.className = classType + " " + ((data.lang === km) ? "translated" : "")
    transWord.innerHTML = data.word
    container.appendChild(transWord)

    // This forEach will add in each of the translation's articles
    data.body.forEach((article) => {
        // Adds the word type/ part of speech
        transWordType = document.createElement("p")
        transWordType.innerHTML = "<small><i>&ensp;" + article.class + "</i></small>"
        container.appendChild(transWordType)
        
        articleElem = document.createElement("p")
        // Determines whether to translate the text or not
        articleElem.className = classType + " " + ((article.lang === km) ? "translated" : "")
        articleElem.innerHTML = "<small>&emsp;" + article.text + "</small>"
        container.appendChild(articleElem)
    })
    
    container.appendChild(document.createElement("hr"))
}

/*
    displayFailedEntry will clear the searchResult paragraph and display the error message
*/
function displayFailedEntry() {
    alert("Unable to find entry.")
}

if (sessionStorage.searchTerm !== null) {
    loadEntry(sessionStorage.searchTerm, en)
}