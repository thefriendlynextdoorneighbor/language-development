class Character {
    constructor(char, font, engName, aspects, rep, pronunciations, pngCount) {
        if(arguments.length == 7) {
            this.rep = rep;
            this.pronunciations = pronunciations;
            this.pngCount = pngCount;
        }
        if(arguments.length == 6) {
            this.rep = rep;
            this.pronunciations = pronunciations;
            this.pngCount = 2;
        }
        if(arguments.length == 5) {
            this.rep = rep;
            this.pronunciations = [this.rep.toLowerCase()];
            this.pngCount = 2;
        }
        if(arguments.length == 4) {
            this.rep = font;
            this.pronunciations = [this.rep.toLowerCase()];
            this.pngCount = 2;
        }
        this.char = char;
        this.font = font;
        this.engName = engName;
        this.aspects = aspects;
    }
}

const chars = {
    "vowels": [
        new Character("asi","a","Ashi",["Truth","Actuality"],["a","b","c"],["a","a&#596;","&#596;a"],3),
        new Character("ez","e","Ezh",["Joy","Sadness","Feeling (Soft)"],["e","f","g"],["&#603;","&#603;a","a&#603;"],3),
        new Character("ixi","i","Itzi",["Anger","Fear","Feeling (Hard)"],["i","j","k"],["i","ia","ai"],3),
        new Character("oLM","o","Olm",["Untruth","Fiction"],["o","p","q"],["&#629;","&#629;i","i&#629;"],3),
        new Character("uN","u","Un",["Tranquility","Peace"],["u","v","w"],["&#649;","&#649;i","i&#649;"],3),
        new Character("lnu","l","Einyu",["Darkness","Tainted","Shadowed"],["l"],["&#603;i"],1),
        new Character("mV","m","i",["Light","Purity"],["m"],["ij&#603;"],1)
    ],
    "consonants": [
        new Character("taRM","t","Tharm",["Bravery","Courage","Charisma"],"t",["&#952;"]),
        new Character("KkB","K","Kaib",["Wisdom","Knowledge"],"K",["k"]),
        new Character("QaLM","Q","Kwalm",["Skill","Mastery","Ability"],"Q",["q"]),
        new Character("FoS","F","Fos",["Force","Strength"],"F",["f"]),
        new Character("slD","s","Sheid",["Cunning","Wit","Persuasiveness"],"s",["&#643;"]),
        new Character("TwN","T","Tyun",["Ambitiousness","Resourcefulness"],"T",["t"]),
        new Character("RjC","R","Riach",["Creativity","Artistry"],"R",["&#638;"]),
        new Character("Geb","G","Geb",["Clarity","Honesty","Sincerity"],"G",["g"]),
        new Character("Jf","J","Jea",["Mystery","Mystique"],"J",["&#669;"]),
        new Character("BbD","B","Baod",["Empathy","Generosity"],"B",["b"]),
        new Character("nuM","n","&Ntilde;um",["Tactics","Strategy"],"n",["&#626;"]),
        new Character("WaK","W","Wak",["Prestige","Royalty"],"W",["&#651;"]),
        new Character("Hos","H","Hosh",["Spirituality","Realization"],"H",["x"]),
        new Character("zm","z","Zhie",["Honor","Pride"],"z",["&#658;"]),
        new Character("Cw","C","Chiu",["Confidence","Brashness","Boldness"],"C",["t&#865;&#643;"]),
        new Character("PaF","P","Paf",["Patience","Tolerance"],"P",["p"]),
        new Character("DeZ","D","Dez",["Optimism","Determination","Enthusiasm"],"D",["d"]),
        new Character("Mw","M","Miu",["Genuinity","Authenticity"],"M",["&#625;"]),
        new Character("xot","x","Tzoth",["Loyalty","Devotion","Pesistance"],"x",["t&#865;s"]),
        new Character("ViG","V","Veeg",["Adventurousness","Curiosity"],"V",["v"]),
        new Character("NeB","N","Neb",["Calculation","Intention"],"N",["n"]),
        new Character("SeT","S","Set",["Sharpness","Ingenuity"],"S",["s"]),
        new Character("LoP","L","Lop",["Reliability","Intensity"],"L",["&#621;"]),
        new Character("Zit","Z","Zeeth",["Justice","Fairness","Morality"],"Z",["z"]),
    ]
}